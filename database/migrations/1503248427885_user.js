'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserSchema extends Schema {
  up () {
    this.create('users', (table) => {
      table.increments()
      table.string('email', 254).notNullable()
      table.string('password', 254).notNullable()
      table.integer('role', 1).notNullable().defaultTo(1)
      table.double('breakInMinutes').notNullable().defaultTo(30)
      table.timestamps()
    })
  }

  down () {
    this.drop('users')
  }
}

module.exports = UserSchema
