'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class WorkdaySchema extends Schema {
  up () {
    this.create('workdays', (table) => {
      table.increments()
      table
        .integer('user_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('users')
        .onDelete('Cascade')
      table.bigInteger('work_start').notNullable()
      table.bigInteger('work_end').nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('workdays')
  }
}

module.exports = WorkdaySchema
