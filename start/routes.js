'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.group(() => {
  Route.post('sign_up', 'AuthController.signUp')
  Route.post('sign_in', 'AuthController.signIn')
  Route.get('sign_out', 'AuthController.signOut')
  Route.get('validate', 'AuthController.validate')
}).prefix('api/v1/auth')

Route.group(() => {
  Route.resource('user', 'UserController')
  Route.resource('workday', 'WorkdayController')
}).prefix('api/v1/')

Route.on('/').render('welcome')
