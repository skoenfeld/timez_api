module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  globals: {
    use: true
  },
	parser: 'babel-eslint',
  extends: [
		"eslint:recommended",
    'prettier',
		'plugin:prettier/recommended'
  ],
  plugins: [
    'prettier'
  ],
  // add your custom rules here
  rules: {
		"prettier/prettier": "error",
		"strict": "off",
		"semi": ["error", "never"],
		"comma-dangle": ["error", "never"],
		"class-methods-use-this": "off",
		"global-require": "off",
		"arrow-parens": ["error", "as-needed"],
		"no-param-reassign": ["error", { "props": false }]
	}
};
