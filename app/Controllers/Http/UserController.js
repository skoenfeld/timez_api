'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const User = use('App/Models/User')
const { validateAll } = use('Validator')

/**
 * Resourceful controller for interacting with users
 */
class UserController {
  /**
   * Show a list of all users.
   * GET users
   *
   * @param {object} ctx
   * @param {Response} ctx.response
   */
  async index({ response }) {
    const users = await User.query().fetch()
    return response.json({
      status: 200,
      messages: 'successfully fetched',
      users
    })
  }

  /**
   * Create/save a new user.
   * POST users
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    const rules = {
      email: 'required | email | unique:users, email',
      password: 'required',
      role: 'required | integer'
    }

    const validation = await validateAll(request.all(), rules)

    if (validation.fails()) {
      return response.json({
        status: 406,
        messages: validation.messages()
      })
    } else {
      const user = new User()
      user.email = request.input('email')
      user.password = request.input('password')
      user.role = request.input('role')

      await user.save()
      return response.json({
        status: 201,
        message: 'successfully created',
        user
      })
    }
  }

  /**
   * Display a single user.
   * GET users/:id
   *
   * @param {object} ctx
   * @param {Response} ctx.response
   */
  async show({ params, response }) {
    const user = await User.find(params.id)
    if (user) {
      const workdays = await user.workdays().fetch()
      response.json({
        status: 200,
        message: 'successfully fetched',
        user,
        workdays
      })
    } else {
      response.json({
        status: 404,
        message: 'Could not find the user with id: ' + params.id
      })
    }
  }

  /**
   * Update user details.
   * PUT or PATCH users/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    const rules = {
      email: 'email | unique:users, email, id,' + params.id,
      role: 'integer'
    }

    const validation = await validateAll(request.all(), rules)

    if (validation.fails()) {
      return response.json({
        status: 406,
        messages: validation.messages()
      })
    } else {
      const user = await User.find(params.id)
      if (request.input('password')) {
        user.password = request.input('password')
      }
      if (request.input('email')) {
        user.email = request.input('email')
      }
      if (request.input('role')) {
        user.role = request.input('role')
      }
      await user.save()
      return response.json({
        status: 204,
        message: 'successfully updated',
        user
      })
    }
  }

  /**
   * Delete a user with id.
   * DELETE users/:id
   *
   * @param {object} ctx
   * @param {Response} ctx.response
   */
  async destroy({ params, response }) {
    const user = await User.find(params.id)
    await user.workdays().where('user_id', params.id).delete()
    await user.delete()
    return response.json({
      status: 204,
      message: 'successfully deleted'
    })
  }
}

module.exports = UserController
