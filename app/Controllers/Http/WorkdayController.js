'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const User = use('App/Models/User')
const Workday = use('App/Models/Workday')
const { validateAll } = use('Validator')


/**
 * Resourceful controller for interacting with workdays
 */
class WorkdayController {
  /**
   * Show a list of all workdays.
   * GET workdays
   *
   * @param {object} ctx
   * @param {Response} ctx.response
   */
  async index ({ response }) {
    const workdays = await Workday.query().fetch()

    return response.json({
      status: 200,
      messages: 'successfully fetched',
      workdays
    })
  }

  /**
   * Create/save a new workday.
   * POST workdays
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const rules = {
      userId: 'required | integer',
      workStart: 'required | integer',
      workEnd: 'integer'
    }

    const validation = await validateAll(request.all(), rules)

    if (validation.fails()) {
      return response.json({
        status: 406,
        messages: validation.messages()
      })
    } else {
      const user = await User.findBy('id', request.input('userId'))
      let responseBody
      if(user) {
        const workday = new Workday()
        workday.user_id = request.input('userId')
        workday.work_start = request.input('workStart')
        if(request.input('workEnd')) {
          workday.work_end = request.input('workEnd')
        }
        await user.workdays().save(workday)
        responseBody = {
          status: 201,
          message: 'successfully created',
          workday
        }
      } else {
        responseBody = {
          status: 404,
          message: 'user not found'
        }
      }
      return response.json(responseBody)
    }
  }

  /**
   * Display a single workday.
   * GET workdays/:id
   *
   * @param {object} ctx
   * @param {Response} ctx.response
   */
  async show ({ params, response }) {
    const workday = await Workday.find(params.id)
    const workday_user = await workday.user().fetch()
    if (workday_user) {
      response.json({
        status: 200,
        message: 'successfully fetched',
        workday,
        workday_user
      })
    } else {
      response.json({
        status: 404,
        message: 'Could not find the workday with id: ' + params.id
      })
    }
  }

  /**
   * Update workday details.
   * PUT or PATCH workdays/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const rules = {
      userId: 'required | integer',
      workStart: 'integer',
      workEnd: 'required | integer'
    }

    const validation = await validateAll(request.all(), rules)

    if (validation.fails()) {
      return response.json({
        status: 406,
        messages: validation.messages()
      })
    } else {
      const workday = await Workday.find(params.id)
      const user = await User.findBy('id', request.input('userId'))
      let responseBody
      if(user && workday) {
        if(request.input('workStart')) {
          workday.work_start = request.input('workStart')
        }
        if(request.input('workEnd')) {
          workday.work_end = request.input('workEnd')
        }

        await user.workdays().save(workday)
        responseBody = {
          status: 201,
          message: 'successfully updated',
          workday
        }
      } else {
        responseBody = {
          status: 404,
          message: 'User or workday not found'
        }
      }
      return response.json(responseBody)
    }
  }

  /**
   * Delete a workday with id.
   * DELETE workdays/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    const workday = await Workday.find(params.id)
    workday.delete()
    return response.json({
      status: 204,
      message: 'successfully deleted'
    })
  }
}

module.exports = WorkdayController
