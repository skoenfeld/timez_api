'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const User = use('App/Models/User')
const { validateAll } = use('Validator')

class AuthController {
  /**
   * Sign Up a new user.
   * POST auth/sign_up
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async signUp({ request, response }) {
    const rules = {
      email: 'required | email | unique:users, email',
      password: 'required | min:8'
    }

    const validation = await validateAll(request.all(), rules)

    if (validation.fails()) {
      return response.json({
        status: 406,
        messages: validation.messages()
      })
    } else {
      const user = new User()
      user.email = request.input('email')
      user.role = request.input('role')
      user.password = request.input('password')
      user.breakInMinutes = request.input('breakInMinutes')

      await user.save()
      return response.json({
        status: 201,
        message: 'successfully signed up',
        user
      })
    }
  }

  /**
   * Sign In a user.
   * POST auth/sign_in
   *
   * @param {object} ctx
   * @param {Request} ctx.auth
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async signIn({ auth, request, response }) {
    const rules = {
      email: 'required',
      password: 'required'
    }

    const validation = await validateAll(request.all(), rules)
    if (validation.fails()) {
      return response.json({
        status: 404,
        messages: validation.messages()
      })
    } else {
      const { email, password } = request.all()
      try {
        const tokenObject = await auth.attempt(email, password)
        if (tokenObject) {
          const currentUser = await User.findBy('email', email)
          response.header('Authorization', tokenObject.token)
          response.json({
            status: 200,
            message: 'sign in successful',
            currentUser,
            tokenObject
          })
        } else {
          response.json({
            status: 404,
            message: 'wrong credentials'
          })
        }
        return response
      } catch (e) {
        response.json({
          status: 404,
          message: 'wrong credentials'
        })
      }
    }
  }

  async validate({ auth, response }) {
    try {
      const currentUser = await auth.getUser()
      return await response.json({
        status: 200,
        currentUser
      })
    } catch (error) {
      return await response.json({
        status: 401,
        message: 'bad token'
      })
    }
  }
}

module.exports = AuthController
